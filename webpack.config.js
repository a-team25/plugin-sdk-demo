const Encore = require('@symfony/webpack-encore');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require("path");

Encore
    .setOutputPath('dist/')
    //.setPublicPath('/dist')
    .setPublicPath(Encore.isProduction() ? 'https://a-dev-team-55.gitlab.io/plugin-sdk-demo/' : '/dist')
    .setManifestKeyPrefix('')
    
    .addEntry('app', './src/js/app.js')
    //.addStyleEntry('style', './src/scss/styles.scss')

    // .copyFiles({
    //     from: './node_modules/font-awesome',
    //     to: './vendor/font-awesome/[path][name].[ext]'
    // })

    .copyFiles({
        from: './src/assets/',
        to: 'images/[path][name].[ext]',
        pattern: /\.(png|jpg|jpeg)$/
    })

/*    .copyFiles({
        from: './node_modules/plus-jakarta-sans/files',
        to: 'fonts/[path][name].[ext]'
    })*/

    .disableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps()
    //.enableVersioning()
    .enableVersioning(Encore.isProduction())

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    .addPlugin(new HtmlWebpackPlugin({
        template: "index.html",
        inject: true,
        minify: {
            removeComments: true,
            collapseWhitespace: true
        }
    }))

    .enableSassLoader()
    .enablePostCssLoader()
;

module.exports = Encore.getWebpackConfig();
