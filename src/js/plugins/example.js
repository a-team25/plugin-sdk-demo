import {Plugin} from '@a-dev-team-55/plugin-sdk/src/plugin-system'

export default class ExamplePlugin extends Plugin {
    init() {
        console.log('Example Plugin geladen ...')
    }

    _onViewportHasChanged() {
        console.log(ViewportDetection.getCurrentViewport())
    }

}
