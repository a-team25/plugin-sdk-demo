// Import our custom CSS
import '../scss/styles.scss'
import '@fontsource-variable/plus-jakarta-sans';

import './fontawesome'

// Import Bootstrap's JS
import { Alert, Tooltip, Toast, Popover } from 'bootstrap'

import PluginSystem from '@a-dev-team-55/plugin-sdk'

PluginSystem.init(true)

import ExamplePlugin from './plugins/example.js';
PluginManager.register('Example', ExamplePlugin, '[data-example]');

import {CountUpPlugin} from '@a-dev-team-55/plugin-sdk/src/plugin'
PluginManager.register('CountUp', CountUpPlugin, '[data-count-up]');

import {ProgressbarPlugin} from '@a-dev-team-55/plugin-sdk/src/plugin'
PluginManager.register('Progressbar', ProgressbarPlugin, '[data-progressbar]');

import {ImageCompareSliderPlugin} from '@a-dev-team-55/plugin-sdk/src/plugin'
PluginManager.register('ImageCompareSlider', ImageCompareSliderPlugin, '[data-image-compare-slider]');
// import 'image-compare-viewer/dist/image-compare-viewer.min.css'
// import 'image-compare-viewer/dist/image-compare-viewer.min.js'

import {TinySliderPlugin} from '@a-dev-team-55/plugin-sdk/src/plugin'
PluginManager.register('TinySlider', TinySliderPlugin, '[data-tiny-slider]');

import {InfoCirclePlugin} from "@a-dev-team-55/plugin-sdk/src/plugin";
PluginManager.register('InfoCircle', InfoCirclePlugin, '[data-info-circle]');

import {TimelineOutputPlugin} from '@a-dev-team-55/plugin-sdk/src/plugin'
PluginManager.register('TimelineOutput', TimelineOutputPlugin, '[data-timeline-output]');

import {TimelinePlugin} from '@a-dev-team-55/plugin-sdk/src/plugin'
PluginManager.register('Timeline', TimelinePlugin, '[data-timeline]');

import {TabmoduleOutputPlugin} from '@a-dev-team-55/plugin-sdk/src/plugin'
PluginManager.register('TabmoduleOutput', TabmoduleOutputPlugin, '[data-tabmodule-output]');

import {TabmodulePlugin} from '@a-dev-team-55/plugin-sdk/src/plugin'
PluginManager.register('Tabmodule', TabmodulePlugin, '[data-tabmodule]');
