import {library, dom} from '@fortawesome/fontawesome-svg-core';

import {
    faStar as faStarSolid,
    faStarHalfStroke as faStarHalfStrokeSolid,
    faArrowRight as faArrowRightSolid,
    faArrowLeft as faArrowLeftSolid,
    faLocationDot as faLocationDotSolid,
    faShieldCat as faShieldCatSolid,
    faGear as faGearSolid,
    faDragon as faDragonSolid,
    faLockOpen as faLockOpenSolid,
    faChevronLeft,
    faChevronRight,
    faArrowDown,
    faArrowUp
} from '@fortawesome/free-solid-svg-icons';

import {
    faStar as faStarRegular,
    faClock as faClockRegular
} from '@fortawesome/free-regular-svg-icons';

library.add(
    faChevronLeft,
    faChevronRight,
    faStarSolid,
    faStarHalfStrokeSolid,
    faArrowRightSolid,
    faArrowLeftSolid,
    faLocationDotSolid,
    faShieldCatSolid,
    faGearSolid,
    faDragonSolid,
    faLockOpenSolid,
    faArrowDown,
    faArrowUp,
    faStarRegular,
    faClockRegular
);

dom.watch();